#Architecture overview
This document is architecture draft about scraping framework.

##Technology stack

###Java 
5 years experience
###groovy 
3 years experience

* scripting language for Java
* can use any Java classes
* added groovy api for easy scripting

###Javascript 
* UI for aggragating scraped data from multiple websites
* using SocketJS for directly bind to vert.x event bus
* binding to vert.x from javascript enable to see aggregated scraped data ASAP

### Vert.x 

 1 year experience, working solution, critical part of our system
 
* [mesasge driven framework for concurrent asynchronous applications](http://vertx.io)
* written in java and simmilar to Akka(Scala), Node.js(Javascript)
* polyglot (can use any JVM language: Java, Groovy, Javascript, JRuby...)
* fully asynchronous, distributed, scalable
* [actor based system](http://en.wikipedia.org/wiki/Actor_model)



###Webdriver
2 year experience, working solution for automating testing web applications

* [framework for automating testing and scraping websites](https://code.google.com/p/selenium/wiki/GettingStarted)
* [W3C standard](http://www.w3.org/TR/2013/WD-webdriver-20130117/)
* developed by google, suceesor of project Selenium
* use GhostDriver(based on PhantomJS) implementation for headless scraping.
*  Suitable for server side web page scraping, fully support of javascript execution.


### MongoDB 
1 year experience

* persistance layer
* storing scraped data in noSQL database
* can use MongoDB Aggregation(Map/reduce) framework for further deep analysis
* for really Big Data use Hadoop based solution such as HBase


### Score24 platform

Could use score24 platform for historical and future match results, events. We have well defined normalized entity relational model of sport industry. We have more than 11 000 unique clubs, 53 000 unique players, 120 000 unique matches, 2 300 000 match events in our database. All enitites and relations could be used as a support knowledge base or onthology. It could be useful for club name disambiguation or normalization.

We have also subscription to 
live data feed such as score changed, red card, yellow card etc. We can get important information as soon as possible. Our entity model 

#Use cases

![image](/Users/teo/Downloads/usecase.png )



# Component diagram

![image](/Users/teo/Downloads/component_.png =800x)

### Description
Each actor is bind to event bus and is allow to send/recieve messages as well. 

Main Vert.x module:

#### Controller actor 
* schedule execution of scraper actor using configurable timer.
* coordinate execution of other actors. 

#### Scraper actor
* execute webdriver script( log in to website, click navigation panel, scrap data)
* Scraper actor communicates with phantom driver process.
* scraper sends scraped data to controller actor


#### Resolver actor
* Resolver actor compare actually scraped data with previous scraped data. Comparison is sent to controller actor.

#### Aggregation simple UI
* User Interface is connected to vert.x event bus throught websocket JS.
* User interface is notified immediately when comparison is sent by Resolver actor. 

#### Interaction actor
* Interaction actor is invoked when user select action from aggregation simple UI.

Whole interaction si described in following sequence diagram.

# Sequence diagram

![image](/Users/teo/Downloads/seq1.png =800x)



#Known issues

Web sites are too huge with lot of ads, popups etc.
Using mobile version of scraped site when possible, ensure shorter load time and simpler page interaction.

Each scraped site may use different match signature. Club names may differ, start time may differ etc. These differences should be eliminated and normalized when aggregating multiple matches from multiple sites.

Scraped website should allow simultaneous logins.


Our system should be deployed so close as scraped website origin. We have good experience with [digital ocean](https://www.digitalocean.com). Final location could be London, Amsterdam or Kuala Lumpur.




