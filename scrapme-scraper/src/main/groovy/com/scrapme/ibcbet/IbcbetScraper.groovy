package com.scrapme.ibcbet

import com.scrapme.selenium.DriverType
import com.scrapme.selenium.SeleniumApi
import groovy.util.logging.Log
import org.openqa.selenium.By
import org.openqa.selenium.WebDriver
import org.openqa.selenium.WebElement
import org.openqa.selenium.support.ui.ExpectedConditions
import org.openqa.selenium.support.ui.WebDriverWait

import java.util.concurrent.TimeUnit

@Log
public class IbcbetScraper {

	static final ROOT_URL = "http://www.ibcbet.com/Default.aspx"


	SeleniumApi api;
	WebDriver d;
	DriverType dt;



	def users = [
		"REU601",
		"REU602",
		"REU603",
		"REU604"
	]

	def pwd = "321Abc"

	public IbcbetScraper(DriverType driverType) {
		dt = driverType;

		api = new SeleniumApi(dt);
		d = api.connect()

		d.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS)
	}

	public void init(){
	}

	public def login() {

		d.get(ROOT_URL)

		d.findElement(By.id("txtID")).sendKeys(users[0]);
		d.findElement(By.id("txtPW")).sendKeys(pwd);

		d.findElement(By.cssSelector("div.loginBtnPos a")).click();
	}

	public void getIframe(final WebDriver driver, final String id=null) {
		final List<WebElement> iframes = driver.findElements(By.tagName("frame"));
		for (WebElement iframe : iframes) {
			log.info(iframe.getAttribute("id"))
		}
		//			if (iframe.getAttribute("id").equals(id)) {
		//			// TODO your stuff.
		//			}

	}

	public void scrap(){

		WebDriverWait wait = new WebDriverWait(d, 15);
		//wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt("mainFrame"))
		sleep(5000)

		getIframe(d)
		d.switchTo().defaultContent()

		d.switchTo().frame("topFrame")

		wait.until(ExpectedConditions.invisibilityOfElementLocated((By.cssSelector("promotion-banner"))))

		println d.findElement(By.id("OddsTr")).text()
	}
}
