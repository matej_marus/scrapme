//package com.scrapme.mongo
//
//import com.mongodb.DB
//import com.mongodb.MongoClient
//import org.jongo.Jongo
//import org.jongo.MongoCollection
//
//public class SbobetRepository extends MongoRepository {
//
//    public static String COL_NAME = "sbobet";
//
//    public static String COL_SPORT_URL = COL_NAME + ".url.sport";
//
//    public static String COL_COUNTRY_URL = COL_NAME + ".url.league";
//
//    public static String COL_LEAGUE_URL = COL_NAME + ".url.league";
//
//    MongoClient mongoClient;
//    DB db;
//    Jongo jongo;
//
//
//    MongoCollection sportUrls;
//    MongoCollection countryUrls;
//    MongoCollection leagueUrls;
//
//    public SbobetRepository() {
//
//        mongoClient = new MongoClient("localhost");
//
//        db = mongoClient.getDB("scrapme");
//
//        jongo = new Jongo(db);
//
//        sportUrls = jongo.getCollection(COL_SPORT_URL);
//        countryUrls = jongo.getCollection(COL_COUNTRY_URL);
//        leagueUrls = jongo.getCollection(COL_LEAGUE_URL);
//    }
//
//    public void insertSportUrl(def url) {
//
//        //db.getCollection(COL_SPORT_URL).insert(url)
//
//        sportUrls.insert(url);
//    }
//
//    public void insertCountryUrl(def url) {
//
//        countryUrls.insert(url);
//    }
//
//
//    public void insertLeague(def url) {
//
//        leagueUrls.insert(url);
//    }
//}
