package com.scrapme.domain;

import org.apache.commons.lang3.StringUtils;

import com.scrapme.sbonet.SbobetParser


public class Record {

	String rowId;
	
	Date date;
	Club sideA;
	Club sideB;

	Double winA;
	Double winB;
	Double draw;
	
	public void buildRowId(String name,Date date){
		
		this.rowId = SbobetParser.dateToString(date)+"-"+StringUtils.lowerCase(sideA?.name?.replaceAll(" ","_"))+"-"+StringUtils.lowerCase(sideB?.name?.replaceAll(" ","_"))
		
		
	}
}