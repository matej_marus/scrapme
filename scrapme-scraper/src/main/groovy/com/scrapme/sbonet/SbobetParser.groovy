package com.scrapme.sbonet

import com.scrapme.domain.Body
import com.scrapme.domain.Club
import com.scrapme.domain.Market
import com.scrapme.domain.Record
import groovy.util.logging.Log
import org.jsoup.Jsoup
import org.jsoup.nodes.Document

import java.text.DateFormat
import java.text.SimpleDateFormat

@Log
public class SbobetParser {

	static final MARKET_1X2 = "1X2"

	static final FIRST_HALF_1X2 = "First Half 1X2"

	static final DATE_PATTERN="MMM dd"

	static final DATE_TIME_PATTERN="MMM dd hh:mm"



	def parseMarket(String name,def date,def m){

		if(MARKET_1X2.equals(name) || FIRST_HALF_1X2.equals(name))
			return parseMarket1x2(name,date,m)
		else
			return parseMarket1x2(name,date,m)
		//return new Market(name)
	}

	def parseDouble(def str){

		try{
			Double.parseDouble(str)
		}catch(Exception ex ){
			//println ex
		}
	}



	def static parseDateTime(String d){
		parseDate(d, DATE_TIME_PATTERN)
	}

	def static dateToString(Date date){
		try {
			DateFormat f = new SimpleDateFormat("YY_MM_DD_HH_MM");
			return f.format(date)
		}catch(Exception ex){
			log.info("unable to parse date: "+date,ex)
		}
		return "unkown";
	}

	def static parseDate(String d,String pattern = DATE_PATTERN){

		Calendar calendar = null
		DateFormat f
		try{
			//def d =s.substring(s.lastIndexOf("/")+1)
			if(d.contains("*:*")){
				pattern = DATE_PATTERN
				d= d.replace("*:*","").trim()

			}
			f = new SimpleDateFormat(pattern);


			calendar =f.getCalendar()

			//			calendar.set(Calendar.HOUR, 0);
			//			calendar.set(Calendar.MINUTE, 0);
			//			calendar.set(Calendar.SECOND, 0);
			//			calendar.set(Calendar.MILLISECOND, 0);
			//TODO set timezone
			//calendar.setTimeZone(TimeZone)
			calendar = SbonetScraper.normalizeDate(calendar)
			calendar.setTime(f.parse(d));
			calendar.set(Calendar.YEAR, Calendar.getInstance().get(Calendar.YEAR));
		}
		catch(Exception ex){
			log.severe("unable to parse date: " + ex)
			calendar=null
		}



		calendar
	}

	def parseMarket1x2(def name,def date,def m){
		def result = [:]
		Market market = new Market()
		market.name = name

		def rows = m.select("div.MarketBd table tr")
		market.records = []
		rows.each{ row->

			Record record = new Record()
			//record.rowId = row.attr("id")
			def cols = row.select("td")
			if(cols !=null && cols.size()>=4){
				String d = cols[0]?.select("div.DateTimeTxt")?.first()?.text()
				if(d!=null)
					date = parseDateTime(d).getTime()
				record.date = date
				//record.date.setTime()
				record.sideA = new Club()
				record.sideA.name= cols[2]?.select("span.OddsL")?.first()?.text()
				record.winA= parseDouble(cols[2]?.select("span.OddsR")?.first()?.text())

				record.draw= parseDouble(cols[3].select("span.OddsR").first()?.text())

				record.sideB = new Club()
				record.sideB.name= cols[4].select("span.OddsL").first()?.text()
				record.winB= parseDouble(cols[4]?.select("span.OddsR").first()?.text())

				record.buildRowId(name,date)

				market.records << record
			}
		}

		market
	}



	public def parse(Date date,String html){

		def result = [:]
		Document doc = Jsoup.parse(html)
		Body body = new Body();
		body.markets = []
		def markets = doc.select("div.MarketT")
		//		result.leagueName=leagueName

		//		result.markets = []
		markets.each{m->
			String marketName = m.select("div.SubHead span").first()?.text()
			Market market = parseMarket(marketName,date, m)
			body.markets << market

		}

		body

		//			def market = [:]
		//			market.name = m.select("div.SubHead span").first()?.text()
		//			market.data = parseMarket(market.name,m)
		//
		//			result.markets << market
		//		}
		//		result
	}
}
