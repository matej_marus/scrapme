package com.scrapme.sbonet

import com.scrapme.domain.CountryUrl
import com.scrapme.domain.DayUrl
import com.scrapme.domain.LeagueUrl
import com.scrapme.domain.SportUrl
import com.scrapme.selenium.DriverType
import com.scrapme.selenium.SeleniumApi
import groovy.util.logging.Log
import org.apache.commons.io.FileUtils
import org.openqa.selenium.*
import org.openqa.selenium.remote.UnreachableBrowserException
import org.openqa.selenium.support.ui.ExpectedCondition
import org.openqa.selenium.support.ui.ExpectedConditions
import org.openqa.selenium.support.ui.WebDriverWait

import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.concurrent.TimeUnit
import java.util.logging.Level

@Log
public class SbonetScraper {

	//TODO conf from db
	public static String ROOT_URL = "http://www.sbobet.com/euro"
	public static String USER = "junkie69"
	public static String PWD = "Amiga500"

	SeleniumApi api;
	WebDriver d;
	DriverType dt;


	//SbobetRepository mongo;

	public SbonetScraper(DriverType driverType) {
		dt = driverType;
		//mongo = new SbobetRepository()

		api = new SeleniumApi(dt);
		d = api.connect()

		d.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS)
	}
	
	public void restart(){
		
//		if(d!=null && d.)
//			d.quit();
		
		d = api.connect()
	}

	public void init(){

	}

	public def login() {

		api = new SeleniumApi(DriverType.ChromeDriver);
		d = api.connect(ROOT_URL);
		d.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS)

		d.findElement(By.id("username")).sendKeys(USER);
		d.findElement(By.id("password")).sendKeys(PWD);

		d.findElement(By.cssSelector("div#account-links a")).click();

		d.findElement(By.cssSelector("input.DWHomeBtn")).click();

		d.findElement(By.cssSelector("input[value='I Agree']")).click();


	}

	public def connect(def url) {

		//d = api.connect()
		d.get(url)
	}

	//	public def parseSport(def url) {
	//
	//		api = new SeleniumApi(dt);
	//
	//		WebDriver driver = api.connect();
	//		driver = driver.get(url)
	//
	//		def result = [:]
	//
	//		def div = driver.findElement(By.cssSelector("div#panel-odds-display"))
	//		//println div.text
	//		println driver.title
	//
	//		driver.close()
	//		driver.quit()
	//
	//		result
	//	}

	def parseSports() {

		d.get(ROOT_URL)
		log.info "navigating $ROOT_URL"

		def result = []
		def entitesEl = d.findElements(By.cssSelector("ul#ms-all li"))

		for (def it : entitesEl) {

			def s = parseSport(it)
			if(s!=null)
				result << s
		}

		result
	}




	def parseSport(def el){

		def scrappedUrl = null

		try {

			def a = el.findElement(By.cssSelector("a"))

			def h = a.getAttribute("href")

			log.info "scraping sport $h"

			scrappedUrl = new SportUrl()
			scrappedUrl.url = h

			def cnt = el.findElement(By.cssSelector("span.NumEvt"))?.getText()
			scrappedUrl.count = Long.parseLong(cnt)

			def txt = a.getText().replaceAll("\n", "").trim()
			scrappedUrl.name = txt.replaceAll(cnt, "")



		} catch (Exception ex) {
			log.log(Level.SEVERE, "scraping sport", ex)
		}

		scrappedUrl
	}



	def parseCountries(def sportsUrl) {
		def result = []

		sportsUrl.each { u ->
			log.info "scraping countries for sport: $u"
			d.get(u.url)

			clickMoreRegions()

			def els = d.findElements(By.cssSelector("ul#ms-all li div ul li"))

			for (def el : els) {
				try {
					def a = el.findElement(By.cssSelector("a"))

					def h = a.getAttribute("href")

					if (h != null) {

						log.info "scraping country $h"

						def scrappedUrl = new CountryUrl()
						//	mongo.insertCountryUrl(scrappedUrl)

						scrappedUrl.url = h


						scrappedUrl.sport = u
						def span = el.findElement(By.cssSelector("span.NumEvtCountry"))
						if(span.isDisplayed())
							scrappedUrl.count = parseLong(span.getText())
						else
							scrappedUrl.count= parseLong(getInnerHtml(span))


						def cnt = scrappedUrl.count.toString()

						def txt = a.getText().replaceAll("\n", "").trim()
						scrappedUrl.name = txt.replaceAll(cnt, "")


						result << scrappedUrl

					}

				} catch (Exception ex) {
					log.log(Level.SEVERE, "scraping country", ex)
				}
			}


		}

		result
	}

	def parseLong(String str){
		try{
			Long.parseLong(str)
		}catch(NumberFormatException ex ){
			//println ex
		}

	}

	def parseDate(def s){

		Calendar calendar = null
		try{
			def d =s.substring(s.lastIndexOf("/")+1)

			DateFormat f = new SimpleDateFormat("yyyy-MM-dd");


			calendar =f.getCalendar()

			calendar = normalizeDate(calendar)

			calendar.setTime(f.parse(d));// all done


		}
		catch(Exception ex){
			log.log(Level.SEVERE, "parsing date: $s", ex)
		}
		calendar
	}

	static def normalizeDate(Calendar calendar){

		calendar.set(Calendar.HOUR, 0);
		calendar.set(Calendar.MINUTE, 0);
		calendar.set(Calendar.SECOND, 0);
		calendar.set(Calendar.MILLISECOND, 0);

		calendar.setTimeZone(TimeZone.getTimeZone("Asia/Hong_Kong"))

		calendar
	}

	def parseDays(def el){

		def result = [:]

		def els = el.findElements(By.cssSelector("tr td a"))

		els.each{ a->

			def h = a.getAttribute("href")
			def s = extract(a.getText())
			if(h!=null && s!=null && s.count>0){

				try{
					log.info "scraping day $h"

					def day = new DayUrl()
					day.url = h

					day.count = s.count
					day.name = s.text

					Calendar calendar = Calendar.getInstance();


					if("Today".equals(day.name))
						day.date = calendar.getTime()

					else if("More".equals(day.name)){
						calendar.add(Calendar.DAY_OF_YEAR, 7);
						//day.date = calendar.getTime()
					}
					else{
						calendar = parseDate(day.url)
					}

					calendar = normalizeDate(calendar)

					day.date = calendar.getTime()

					result[day.date]=day
				}
				catch(Exception ex){
					log.log(Level.SEVERE, "unable to scrap day: $h", ex)
				}

			}

		}

		result

	}

	def extract(String s){
		if(s==null)
			return null

		log.info "extracting count: $s"
		def result = [:]

		result.count = 0

		def pattern = "\\([0-9]*\\)"
		def matches = s =~ pattern
		if(matches!=null && matches.size()>0){
			result.text = s.replaceAll(pattern, "").trim()
			result.count = parseLong(matches[0].replaceAll("\\(","").replaceAll("\\)",""))
		}

		result
	}

	def parseSportChallenges(def countiesUrl) {
		def result = []

		countiesUrl.each { u ->

			log.info "scraping leagues for country: $u"
			d.get(u.url)

			//			clickMoreRegions()


			def els = d.findElements(By.cssSelector("ul#ms-all li div ul li div ul li"))

			for (def el : els) {
				try {

					result << parseLeague(el,u)


				} catch (Exception ex) {
					log.log(Level.SEVERE, "parse league", ex)
				}
			}

		}

		result.each{ league->
			d.get(league.url)
			league.days = parseDays(d.findElement(By.cssSelector("table.TimeTab")))

		}

		result
	}

	private def parseLeague(def el,def u){
		def a = el.findElement(By.cssSelector("a"))

		def h = a.getAttribute("href")

		def scrappedUrl = null

		if (h != null) {

			log.info "scraping league $h"
			scrappedUrl = new LeagueUrl()
			//	mongo.insertCountryUrl(scrappedUrl)

			scrappedUrl.url = h

			scrappedUrl.country = u
			def leagueName = a.getText()
			def s = extract(leagueName)

			//	scrappedUrl.days = parseDays(d.findElement(By.cssSelector("table.TimeTab")))

			scrappedUrl.count = s.count

			scrappedUrl.name = s.text

		}

		scrappedUrl
	}



	public def refreshBody(){
		def refreshBtn = d.findElement(By.cssSelector("div.RefreshBtn"))
		refreshBtn.click()

	}

	public def parseBody(String url){

		def result =""
//		d.quit()
		try{
			d.get(url)
			
					
			//			sleep(5000)
			//			File scrFile = ((TakesScreenshot)d).getScreenshotAs(OutputType.FILE);
			//
			//			FileUtils.copyFile(scrFile, new File("./screenshot.png"));

			if(!url.contains("outright")){
				def all = d.findElements(By.cssSelector("div#panel-odds-display div div div a")).get(3)

				all.click()

				waitFor(By.cssSelector("div#panel-odds-display div div div a.Sel"),"All")
			}
			def body = null;

			try{
				body = d.findElement(By.cssSelector("div.NonLiveMarket"))
			}
			catch(org.openqa.selenium.NoSuchElementException e){

				body = d.findElement(By.cssSelector("div.LiveMarket"))
			}
			if(body!=null)
				//result = (String)((JavascriptExecutor)d).executeScript("return arguments[0].outerHTML;", body);
				result = getHtml(body)

		}
		catch(UnreachableBrowserException ex){

			log.severe "phantomjs die, restarting"
				restart();
			
			
			}
		catch(WebDriverException ex){
		if(ex.message.contains("died")){
			log.severe "phantomjs die, restarting"
			restart();
		}
		
		}catch(Exception ex){
			log.log(Level.SEVERE, "unable to parse body: $url", ex)
			String fileName= "./screenshots/${url.substring(url.lastIndexOf('/')+1)}"+".png"
			File scrFile = ((TakesScreenshot)d).getScreenshotAs(OutputType.FILE);
			FileUtils.copyFile(scrFile, new File(fileName));
			log.info "screenshot taken to: $fileName"
		}
		result
	}


	public static ExpectedCondition<Boolean> hasCssClass(
			final WebElement element, final String c) {

		return new ExpectedCondition<Boolean>() {
					@Override
					public Boolean apply(WebDriver driver) {
						try {
							String cls = element.getAttribute("class");

							if (cls != null) {
								return cls.contains(c)
							} else {
								return false;
							}
						} catch (StaleElementReferenceException e) {
							return null;
						}
					}

					@Override
					public String toString() {
						return String.format("class ('%s') to be present in element %s", c, element);
					}
				};
	}


	public void waitFor(def by,def txt){
		WebDriverWait wait = new WebDriverWait(d, 5);
		wait.until(ExpectedConditions.textToBePresentInElementLocated(by, txt))
		//wait.until(hasCssClass(element,c));
	}

	public def getHtml(def el){
		(String)((JavascriptExecutor)d).executeScript("return arguments[0].outerHTML;", el);
	}

	public def getInnerHtml(def el){
		(String)((JavascriptExecutor)d).executeScript("return arguments[0].innerHTML;", el);
	}

	public def parseMenu() {



		def all = []

		def sportsUrl = parseSports()
		//	println sportsUrl
		//mongo.insertSportUrl(sportsUrl)

		def countryUrls = parseCountries(sportsUrl)
		//	println countryUrls
		//mongo.insertCountryUrl(countryUrls)

		def sportChallengeUrls = parseSportChallenges(countryUrls)
		//	mongo.insertLeague(sportChallengeUrls)
		//	println sportChallengeUrls

		//all.addAll(sportsUrl)
		//all.addAll(countryUrls)
		all.addAll(sportChallengeUrls)

		d.close();
		d.quit();
		//	println all.size()
		all

		//		def headers = d.findElements(By.cssSelector(".SubHeadT"))
		//		headers.each {h->
		//			println h.getText()
		//
		//		}
		//
		//		def tables = d.findElements(By.cssSelector("table.Hdp"))
		//		tables.each {h->
		//			println h.getText()
		//		}
		//SubHeadT
	}

	def clickMoreRegions() {
		try {
			def moreRegions = d.findElement(By.cssSelector("li.MoreReg"))
			moreRegions.click()
		} catch (Exception e) {
			log.info "unable to click more regions"
		}
	}

	def quit() {
		if (api != null && d!=null) {
			d.close()
			d.quit()
		}
	}
}
