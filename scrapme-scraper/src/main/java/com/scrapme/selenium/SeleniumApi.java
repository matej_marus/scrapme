package com.scrapme.selenium;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.phantomjs.PhantomJSDriver;
import org.openqa.selenium.phantomjs.PhantomJSDriverService;
import org.openqa.selenium.remote.DesiredCapabilities;

public class SeleniumApi {

	private DriverType type;

	public SeleniumApi(DriverType type) {
		this.type = type;

	}
	
	

	public WebDriver connect() {

		WebDriver driver = null;
		if (type.equals(DriverType.ChromeDriver)) {

			System.setProperty("webdriver.chrome.driver",
					"/usr/local/share/chromedriver");

			System.setProperty("webdriver.chrome.logfile", "./chromedriver.log");

			driver = new ChromeDriver();

		} else if (type.equals(DriverType.GhostDriver)) {
			DesiredCapabilities caps = new DesiredCapabilities();
			caps.setCapability(
					PhantomJSDriverService.PHANTOMJS_EXECUTABLE_PATH_PROPERTY,
					"/usr/local/bin/phantomjs");
			// new String[] { "--ignore-ssl-errors=yes", "--load-images=no" }
			caps.setCapability(PhantomJSDriverService.PHANTOMJS_CLI_ARGS,
					new String[] { "--ignore-ssl-errors=yes" });
			driver = new PhantomJSDriver(caps);

		} else
			throw new IllegalArgumentException("unknow driver");

		return driver;
	}
}
