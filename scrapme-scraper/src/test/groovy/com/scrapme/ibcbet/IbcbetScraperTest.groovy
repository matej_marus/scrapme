package com.scrapme.ibcbet

import groovy.json.JsonBuilder

import org.apache.commons.io.FileUtils
import org.json.JSONObject
import org.junit.Test
import org.openqa.selenium.OutputType
import org.openqa.selenium.TakesScreenshot
import org.openqa.selenium.WebDriver
import org.openqa.selenium.phantomjs.PhantomJSDriver
import org.openqa.selenium.phantomjs.PhantomJSDriverService
import org.openqa.selenium.remote.DesiredCapabilities
import org.slf4j.Logger
import org.slf4j.LoggerFactory

import com.scrapme.domain.DayUrl
import com.scrapme.domain.LeagueUrl
import com.scrapme.selenium.DriverType

public class IbcbetScraperTest {

	final static Logger log = LoggerFactory.getLogger(IbcbetScraperTest.class);


	@Test
	public void testLogin(){

		IbcbetScraper scraper = new IbcbetScraper(DriverType.ChromeDriver)

		scraper.login()
	}

	@Test
	public void testScrap(){

		IbcbetScraper scraper = new IbcbetScraper(DriverType.ChromeDriver)

		scraper.login()

		scraper.scrap()
	}
}
