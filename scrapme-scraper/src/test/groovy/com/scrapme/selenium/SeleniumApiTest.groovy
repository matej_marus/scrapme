package com.scrapme.selenium;

import java.util.concurrent.TimeUnit

import org.junit.Test
import org.openqa.selenium.By
import org.openqa.selenium.JavascriptExecutor
import org.openqa.selenium.WebDriver

public class SeleniumApiTest {

	@Test
	public void startipTest() {

		SeleniumApi api = new SeleniumApi(DriverType.ChromeDriver);
		WebDriver d = api.connect("http://www.startitup.sk/freelancers/?#f193-peter-privalinec");
		d.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS)
		((JavascriptExecutor)d).executeScript("showContact('freelancer','tuximail@gmail.com');");
		d.findElement(By.cssSelector("input[name=ContactName]")).sendKeys("H3ck3r");
		d.findElement(By.cssSelector("input[name=ContactSubject]")).sendKeys("Hi");
		d.findElement(By.cssSelector("input[name=ContactEmail]")).sendKeys("Hi");
		d.findElement(By.cssSelector("textarea[name=ContactMessage]")).sendKeys("Hi");
		d.findElement(By.cssSelector("a.submitContactForm")).click();
	}

	@Test
	public void wizzairTest() {

		SeleniumApi api = new SeleniumApi(DriverType.GhostDriver);
		WebDriver d = api.connect("https://wizzair.com/cs-CZ/Search");
		d.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS)
		d.findElement(By.cssSelector("input.city-from")).click();
		d.findElement(By.cssSelector("ul li[data-iata=CRL]")).click();

		//
		d.findElement(By.cssSelector("ul li[data-iata=BUD]")).click();
		d.findElement(By.cssSelector("div.promocode-search-flights-button button")).click();
	}

	@Test
	public void sbonetTest() {

		SeleniumApi api = new SeleniumApi(DriverType.Web);
		WebDriver d = api.connect("https://www.sbobet.com/euro");
		d.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS)
		d.findElement(By.id("username")).sendKeys("junkie69");
		d.findElement(By.id("password")).sendKeys("Amiga500");
		d.findElement(By.cssSelector("div#account-links a")).click();

		d.findElement(By.cssSelector("input.DWHomeBtn")).click();
		//OddsR
		//OddsL
		def headers = d.findElements(By.cssSelector(".SubHeadT"))
		headers.each {h->
			println h.getText()

		}

		def tables = d.findElements(By.cssSelector("table.Hdp"))
		tables.each {h->
			println h.getText()
		}
		//SubHeadT
	}
}
