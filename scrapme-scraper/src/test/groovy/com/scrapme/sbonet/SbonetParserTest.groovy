package com.scrapme.sbonet

import groovy.json.JsonBuilder

import org.apache.commons.io.FileUtils
import org.json.JSONObject
import org.junit.Test
import org.openqa.selenium.OutputType
import org.openqa.selenium.TakesScreenshot
import org.openqa.selenium.WebDriver
import org.openqa.selenium.phantomjs.PhantomJSDriver
import org.openqa.selenium.phantomjs.PhantomJSDriverService
import org.openqa.selenium.remote.DesiredCapabilities
import org.slf4j.Logger
import org.slf4j.LoggerFactory

import com.scrapme.domain.Club
import com.scrapme.domain.DayUrl
import com.scrapme.domain.LeagueUrl
import com.scrapme.domain.Market
import com.scrapme.domain.Record
import com.scrapme.selenium.DriverType

public class SbonetParserTest {

	final static Logger log = LoggerFactory.getLogger(SbonetScraper.class);


	@Test
	public void testLogin(){

		SbonetScraper parser= new SbonetScraper()
		parser.login()
	}

	@Test
	public void testclickMoreReg() {

		SbonetScraper parser = new SbonetScraper(DriverType.ChromeDriver)
		parser.connect("https://www.sbobet.com/euro/football")
		parser.clickMoreRegions()
		//parser.quit()
	}


	@Test
	public void parseDateFromUrl(){
		SbonetScraper parser = new SbonetScraper(DriverType.ChromeDriver)
		def d = parser.parseDate("https://www.sbobet.com/euro/football/germany-bundesliga-2/2014-11-14")
		println d
	}

	@Test
	public void testParseCountries() {

		SbonetScraper parser = new SbonetScraper(DriverType.GhostDriver)

		def sports = parser.parseSports()

		def result = parser.parseCountries(sports[0..0])
		println result
	}

	//	@Test
	//	public void testParseSport() {
	//
	//		SbonetScraper parser = new SbonetScraper(DriverType.ChromeDriver)
	//		parser.connect("https://www.sbobet.com/euro/football")
	//
	//		println result
	//	}


	@Test
	public void testRegexp() {

		def matches = "English Johnstones Paint Trophy (6)" =~ "\\([0-9]*\\)"
		println matches[0]
	}



	@Test
	public void testParseSportChallenges() {

		SbonetScraper parser = new SbonetScraper(DriverType.GhostDriver)

		def sports = parser.parseSports()

		def countries = parser.parseCountries(sports[0..0])

		def result = parser.parseSportChallenges(countries[0..0])
		assert result[0].days.size() >0

		println result




	}

	@Test
	public void testJson(){
		SbonetScraper parser = new SbonetScraper(DriverType.GhostDriver)

		def sports = parser.parseSports()

		def countries = parser.parseCountries(sports[0..0])

		def result = parser.parseSportChallenges(countries[0..1])

		println new JsonBuilder( result ).toPrettyString()

	}

	@Test
	public void testRefresh() {

		SbonetScraper parser= new SbonetScraper(DriverType.ChromeDriver)
		parser.connect("https://www.sbobet.com/euro/football/english-championship")
		sleep(5000)
		parser.refreshBody()
	}

	@Test
	public void testMap() {

		LeagueUrl root = new LeagueUrl()
		root.name = "test"

		Date k1=new Date()
		k1.hours=0
		k1.minutes=0
		k1.seconds=0

		root.days = [:];
		root.days[k1] = new DayUrl()
		root.days[k1].url ="k1"
		root.days[k1].name ="name"

		Date k2=new Date().plus(7);
		root.days[k2] = new DayUrl()
		root.days[k2].url ="k1"
		root.days[k2].name ="name"

		//
		//		root.days[k2] = new DayUrl()
		//		root.days[k2].url ="k2"
		//
		//println root
		JSONObject jsonObj = new JSONObject( root );
		System.out.println( jsonObj );


	}

	@Test
	public void testTimeZone() {

		String d = "Nov 01 4:00"
		def calendar = SbobetParser.parseDateTime(d)

		assert calendar.getTimeZone()==TimeZone.getTimeZone("Asia/Hong_Kong")

		println calendar

	}

	@Test
	public void testParseDate() {

		String d = "Nov 01"
		def calendar = SbobetParser.parseDate(d)

		println calendar.get(Calendar.MONTH);
		println calendar.get(Calendar.YEAR);
		println calendar.get(Calendar.DAY_OF_MONTH);

	}

	@Test
	public void testIncorrectParseDateTime() {

		String d = "Nov 26 *:*"
		def calendar = SbobetParser.parseDateTime(d)
		assert calendar
		println calendar.get(Calendar.MONTH);
		println calendar.get(Calendar.YEAR);
		println calendar.get(Calendar.DAY_OF_MONTH);
		println calendar.get(Calendar.HOUR_OF_DAY);
		println calendar.get(Calendar.MINUTE);

	}

	@Test
	public void testParseDateTime() {

		String d = "Nov 26 08:15"
		def calendar = SbobetParser.parseDateTime(d)

		println calendar.get(Calendar.MONTH);
		println calendar.get(Calendar.YEAR);
		println calendar.get(Calendar.DAY_OF_MONTH);
		println calendar.get(Calendar.HOUR_OF_DAY);
		println calendar.get(Calendar.MINUTE);

	}

	@Test
	public void testRowId() {

		Market m = new Market();
		m.name= "1x2"
		Record r = new Record();
		r.sideA = new Club();
		r.sideA.name="FC liverpoool";
		
		r.sideB = new Club();
		r.sideB.name="Chealsa London";
		r.buildRowId(m.name,new Date())
		log.info(r.rowId)
	}
	
	@Test
	public void testDateToString() {
		log.info(SbobetParser.dateToString(new Date()))
		
	}
	
	@Test
	public void testParseBody() {

		SbonetScraper parser= new SbonetScraper(DriverType.GhostDriver)
		log.info("webdriver initialized")
	//	Thread.sleep(15000)
		def body = parser.parseBody("https://www.sbobet.com/euro/football/uefa-europa-league---which-team-will-advance-to-next-round/more")
		log.info("scraped")
		 body = parser.parseBody("https://www.sbobet.com/euro/football/uefa-europa-league---which-team-will-advance-to-next-round/more")
		log.info("scraped2")
		assert body.contains("Asian Handicap")
		println body
	}

	@Test
	public void testScreenshot() {

		SbonetScraper parser= new SbonetScraper(DriverType.GhostDriver)
		log.info("webdriver initialized")
	
		 body = parser.parseBody("https://www.google.sk")
		log.info("scraped")
	
		println body
	}

	
	@Test
	public void testParseBodyPerformance() {

		SbobetParser p = new SbobetParser()
		def result = null
		SbonetScraper parser= new SbonetScraper(DriverType.GhostDriver)
		log.info("webdriver initialized")
		def body = parser.parseBody("https://www.sbobet.com/euro/football/germany-bundesliga-2")
		log.info("scraped")
		p.parse(new Date(),body)
		log.info("parsed")
		body = parser.parseBody("https://www.sbobet.com/euro/ice-hockey/united-states")
		log.info("scraped")
		p.parse(new Date(),body)
		log.info("parsed")
		body = parser.parseBody("https://www.sbobet.com/euro/football/english-premier-league")
		log.info("scraped")
		p.parse(new Date(),body)
		log.info("parsed")
		body = parser.parseBody("https://www.sbobet.com/euro/football/english-league-two")
		log.info("scraped")
		p.parse(new Date(),body)
		log.info("parsed")
		body = parser.parseBody("https://www.sbobet.com/euro/football/germany-bundesliga-2")
		log.info("scraped")
		p.parse(new Date(),body)
		log.info("parsed")
		body = parser.parseBody("https://www.sbobet.com/euro/ice-hockey/united-states")
		log.info("scraped")
		p.parse(new Date(),body)
		log.info("parsed")
		body = parser.parseBody("https://www.sbobet.com/euro/football/english-premier-league")
		log.info("scraped")
		p.parse(new Date(),body)
		log.info("parsed")
		body = parser.parseBody("https://www.sbobet.com/euro/football/english-league-two")
		log.info("scraped")
		p.parse(new Date(),body)
		log.info("parsed")
	}

	@Test
	public void testParseHtml(){

		String html = this.getClass().getResourceAsStream("/html/sbobet.html").getText()
		SbobetParser parser = new SbobetParser()
		def result = parser.parse(new Date(),html)
		println new JsonBuilder( result ).toPrettyString()
		//println result
		assert result!=null
	}

	@Test
	public void testParseSports() {

		SbonetScraper parser= new SbonetScraper(DriverType.GhostDriver)
		parser.init()
		def sports = parser.parseSports()

		println sports

	}

	@Test
	public void testParseMenu() {

		SbonetScraper parser= new SbonetScraper(DriverType.GhostDriver)

		def urls = parser.parseMenu()
		println urls

		//		def parsers = []
		//		urls.each { url->
		//			parsers<< [
		//				p:new SbonetParser(DriverType.GhostDriver),
		//				u:url
		//			]
		//		}
		//
		//		sleep(10000)
		//
		//		GParsExecutorsPool.withPool{
		//			parsers.eachParallel{ p->
		//
		//				p.p.parseSport(p.u)
		//			}
		//		}
	}

	@Test
	public void testParse(){
		DesiredCapabilities caps = new DesiredCapabilities();
		caps.setCapability(
				PhantomJSDriverService.PHANTOMJS_EXECUTABLE_PATH_PROPERTY,
				"/usr/local/bin/phantomjs");
		caps.setCapability(PhantomJSDriverService.PHANTOMJS_CLI_ARGS,[
			"--ignore-ssl-errors=yes"
		]);

		WebDriver	driver = new PhantomJSDriver(caps);
		def u = "https://www.sbobet.com"
		//def u = "https://www.google.com"
		driver.get(u)
		println driver.getCurrentUrl()
		File scrFile = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
		sleep(5000)
		FileUtils.copyFile(scrFile, new File("./screenshot.png"));

		println driver.getPageSource()
	}


	@Test
	public void testParseSport(){

		SbonetScraper parser= new SbonetScraper(DriverType.GhostDriver)
		def result = parser.parseSport("https://www.sbobet.com/euro/football")
		println result
	}
}
