import com.scrapme.core.Address;
import io.vertx.core.DeploymentOptions;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.test.core.VertxTestBase;
import org.junit.Test;

import java.util.concurrent.CountDownLatch;

/**
 * Created by matko on 7.11.2014.
 */
public class SbonetTest extends VertxTestBase {


    @Test
    public void getMenuTest() {
        vertx.eventBus().send(Address.SBOBET_MENU.a, null, onSuccess(resp -> {

            JsonArray array = (JsonArray) resp.body();
            assertNotNull(array);
            testComplete();
        }));
        await();
    }


    @Override
    public void setUp() throws Exception {
        super.setUp();
        CountDownLatch latch = new CountDownLatch(1);
        vertx.deployVerticle("java:com.scrapme.core.sbobet.SbobetManager", new DeploymentOptions(), onSuccess(res -> {
            vertx.deployVerticle("service:io.vertx:mongo-service", new DeploymentOptions().setConfig(new JsonObject().put("address", Address.MONGO.toString()).put("db_name", "scrapme")), onSuccess(s -> {
                System.out.println("sbobet Started");
                latch.countDown();
            }));
        }));
        awaitLatch(latch);
    }
}
