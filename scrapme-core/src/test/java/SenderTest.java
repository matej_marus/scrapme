import com.scrapme.core.Address;
import io.vertx.core.buffer.Buffer;
import io.vertx.test.core.VertxTestBase;
import org.junit.Test;

import java.util.concurrent.CountDownLatch;

/**
 * Created by matko on 16.12.2014.
 */
public class SenderTest extends VertxTestBase {

    @Test
    public void testBlank() {
        vertx.eventBus().send(Address.CATO_ENDPOINT.a, "{}", onSuccess(result -> {
            Buffer body = (Buffer) result.body();
            assertEquals("OK", body.toString());
            testComplete();
        }));
        await();
    }

    @Test
    public void testFail() {
        vertx.eventBus().send(Address.CATO_ENDPOINT.a, "", onSuccess(result -> {
            Buffer body = (Buffer) result.body();
            assertNotSame("OK", body.toString());
            testComplete();
        }));
        await();
    }

    @Override
    public void setUp() throws Exception {
        super.setUp();
        disableThreadChecks();
        CountDownLatch latch = new CountDownLatch(1);
        vertx.deployVerticle("java:com.scrapme.core.Sender", onSuccess(res -> {
            System.out.println("sender Started");
            latch.countDown();
        }));
        awaitLatch(latch);
    }
}
