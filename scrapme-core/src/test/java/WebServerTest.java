import io.vertx.core.Vertx;
import io.vertx.core.VertxOptions;
import io.vertx.test.core.VertxTestBase;

import java.util.concurrent.CountDownLatch;

/**
 * Created by matko on 23.9.2014.
 */
public class WebServerTest extends VertxTestBase {


    @Override
    public void setUp() throws Exception {
        super.setUp();
        CountDownLatch latch = new CountDownLatch(1);
        Vertx.vertx(new VertxOptions().setHAEnabled(true).setMaxWorkerExecuteTime(1000000000000l)).deployVerticle("java:com.scrapme.core.WebServer", onSuccess(res -> {
            System.out.println("server Started");
            latch.countDown();
        }));
        awaitLatch(latch);
    }

}
