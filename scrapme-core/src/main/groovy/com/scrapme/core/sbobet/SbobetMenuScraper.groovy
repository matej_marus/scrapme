package com.scrapme.core.sbobet

import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.scrapme.core.Address
import com.scrapme.sbonet.SbonetScraper
import com.scrapme.selenium.DriverType
import groovy.util.logging.Log
import io.vertx.core.Future
import io.vertx.core.json.JsonArray
import io.vertx.lang.groovy.GroovyVerticle

/**
 * Created by matko on 12.11.2014.
 */
@Log
class SbobetMenuScraper extends GroovyVerticle {

    private static final String DATE_PATTERN = "EEE MMM dd HH:mm:ss z yyyy";
    SbonetScraper parser
    Gson gson
    @Override
    void start(Future<Void> startFuture) throws Exception {

        log.info "SbobetMenuScraper started"
        parser = new SbonetScraper(DriverType.GhostDriver)
        gson = new GsonBuilder().setDateFormat(DATE_PATTERN).create();
        vertx.eventBus().consumer(Address.SBOBET_MENU.a, handler)
        startFuture.complete()
    }

    def handler = { msg ->
        def resp = [:]

        long current = System.currentTimeMillis()
        def sport = parser.parseSports()
        log.info("sport menu parsed in ${System.currentTimeMillis() - current}")
        def country = parser.parseCountries(sport.get(0))
        def league = parser.parseSportChallenges(country)

        resp.sport = new JsonArray(gson.toJson(sport))
        resp.country = new JsonArray(gson.toJson(country))
        resp.leagues = gson.toJson(league)
        msg.reply(resp)
    }

    @Override
    void stop(Future<Void> stopFuture) throws Exception {
        parser.quit()
        stopFuture.complete()
    }
}
