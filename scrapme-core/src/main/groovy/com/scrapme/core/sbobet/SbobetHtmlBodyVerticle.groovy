package com.scrapme.core.sbobet

import com.scrapme.core.Address
import com.scrapme.sbonet.SbonetScraper
import com.scrapme.selenium.DriverType
import io.vertx.core.Future
import io.vertx.core.json.JsonObject
import io.vertx.lang.groovy.GroovyVerticle

/**
 * Created by matko on 6.11.2014.
 */
class SbobetHtmlBodyVerticle extends GroovyVerticle {

    SbonetScraper scraper

    @Override
    void start(Future<Void> startFuture) throws Exception {
        scraper = new SbonetScraper(DriverType.GhostDriver)
        vertx.eventBus().consumer(Address.SBONET_SCRAPPER.a).handler(handler)
        startFuture.complete()

    }

    @Override
    void stop(Future<Void> stopFuture) throws Exception {
        scraper.quit()
        stopFuture.complete()
    }

    def scrap(def url) {
        //   long current = System.currentTimeMillis()
        def body = scraper.parseBody(url)
        //   println("end processBody, it takes: " + (System.currentTimeMillis() - current) + " ms")
        body
    }

    def handler = { msg ->
        JsonObject resp = msg.body()
        resp.put("html", scrap(resp.getString("url")))
        msg.reply(resp)
    }
}
