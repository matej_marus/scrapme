package com.scrapme.core.sbobet

import com.google.gson.Gson
import com.scrapme.core.Address
import com.scrapme.sbonet.SbobetParser
import groovy.util.logging.Log
import io.vertx.core.Future
import io.vertx.core.json.JsonObject
import io.vertx.lang.groovy.GroovyVerticle

/**
 * Created by matko on 30.11.2014.
 */
@Log
class SbobetParserVerticle extends GroovyVerticle {

    SbobetParser parser
    Gson gson
    @Override
    void start(Future<Void> startFuture) throws Exception {
        parser = new SbobetParser()
        vertx.eventBus().consumer(Address.SBOBET_PARSER.a).handler(handler)
        gson = new Gson()
        startFuture.complete()
    }


    def handler = {
        //  long current = System.currentTimeMillis()
        JsonObject msg = it.body()
        try {
            msg.put("json", gson.toJson(parser.parse(new Date(), msg.getString("html"))))
        } catch (e) {
            log.severe(e.message)
        }
        //println("end processParser, it takes: " + (System.currentTimeMillis() - current) + " ms")
        it.reply(msg)
    }
}
