package com.scrapme.core;

/**
 * Created by matko on 7.11.2014.
 */
public enum Address {

    SBONET_SCRAPPER("SBONET_SCRAPPER"), MONGO("mongoservice"), SBOBET_PARSER("sbobet_parser"), SBOBET_MENU("sbobet_menu"), CATO_ENDPOINT("cato_endpoint"), SYNC_LEAGUE("sync_league");

    public String a;

    private Address(String adr) {
        this.a = adr;
    }


    @Override
    public String toString() {
        return a;
    }
}
