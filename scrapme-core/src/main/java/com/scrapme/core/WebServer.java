package com.scrapme.core;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.DeploymentOptions;
import io.vertx.core.Future;
import io.vertx.core.http.HttpServer;
import io.vertx.core.http.HttpServerOptions;
import io.vertx.core.json.JsonObject;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.impl.LoggerFactory;
import io.vertx.ext.sockjs.BridgeOptions;
import io.vertx.ext.sockjs.SockJSServer;
import io.vertx.ext.sockjs.SockJSServerOptions;


/**
 * Created by matko on 22.9.2014.
 */
public class WebServer extends AbstractVerticle {

    final Logger log = LoggerFactory.getLogger(WebServer.class);
    @Override
    public void start(Future<Void> startFuture) throws Exception {


        vertx.deployVerticle("service:io.vertx:mongo-service", new DeploymentOptions().setConfig(new JsonObject().put("address", Address.MONGO.toString()).put("db_name", "scrapme")));
        vertx.deployVerticle("java:com.scrapme.core.sbobet.SbobetManager", new DeploymentOptions().setConfig(config().getJsonObject("sbobet")));

        vertx.setPeriodic(60000, period -> log.info(vertx.metrics().get("vertx.eventbus.messages.received").encodePrettily()));
        // Create the HTTP server
        HttpServer server = vertx.createHttpServer(new HttpServerOptions().setHost(config().getString("host", "localhost"))
                .setPort(config().getInteger("port", 8080)));

        setupSockJSBridge(server);

        server.listen(res -> {
            // When the web server is listening we'll say that the start of this verticle is complete
            if (res.succeeded()) {
                startFuture.complete();
            } else {
                startFuture.fail(res.cause());
            }
        });

        log.info("Server is listening");
    }

    @Override
    public void stop(Future<Void> future) throws Exception {

    }

    private void setupSockJSBridge(HttpServer server) {
        // We start a SockJS bridge - this allows the event bus to be used in client side JavaScript!
        SockJSServer sockJSServer = SockJSServer.sockJSServer(vertx, server);

        // We setup the bridge to not allow any inbound messages on the event bus from the client and only outbound
        // messages from the address "example.stocks"
        sockJSServer.bridge(new SockJSServerOptions().setPrefix("/eventbus"),
                new BridgeOptions().addOutboundPermitted(new JsonObject().put("address", "example.stocks")));

    }
}
