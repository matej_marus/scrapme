package com.scrapme.core.sbobet;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.scrapme.core.Address;
import com.scrapme.domain.Body;
import com.scrapme.domain.LeagueUrl;
import com.scrapme.domain.Market;
import com.scrapme.domain.Record;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.AsyncResult;
import io.vertx.core.DeploymentOptions;
import io.vertx.core.eventbus.DeliveryOptions;
import io.vertx.core.eventbus.Message;
import io.vertx.core.json.JsonObject;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.impl.LoggerFactory;
import io.vertx.ext.mongo.MongoService;

import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by matko on 10.11.2014.
 */
public class SbobetManager extends AbstractVerticle {


    private static final String DATE_PATTERN = "EEE MMM dd HH:mm:ss z yyyy";

    public static final String oneXtwo = "1X2";
    public static final String URL = "url";
    public static final String KEY = "key";

    public static final int PROCESS_REPLY_TIMEOUT = 10 * 60 * 1000;
    private int scraperBodyCounter;
    private long lastBodyScrapingTime = 0;

    final Logger log = LoggerFactory.getLogger(SbobetManager.class);

    private MongoService mongoService;
    private Gson gson;

    private Map<String, LeagueUrl> leagueCache = Collections.synchronizedMap(new HashMap<>());
    private Map<String, Record> matchCache = Collections.synchronizedMap(new HashMap<>());

    private static interface CacheLoader {
        void loaded();
    }

    @Override
    public void start() throws Exception {
        gson = new GsonBuilder().setDateFormat(DATE_PATTERN).create();
        mongoService = MongoService.createEventBusProxy(vertx, Address.MONGO.toString());

        if (config().getBoolean("processMenu", true)) {
            log.info("processing Sbobet Menu...");
            vertx.deployVerticle("groovy:com.scrapme.core.sbobet.SbobetMenuScraper", new DeploymentOptions().setWorker(true), deployment -> {
                if (deployment.failed()) {
                    throw new RuntimeException("deployment MenuSrapper failed", deployment.cause());
                }
                processMenu();
                vertx.setPeriodic(config().getInteger("processMenuDelay"), period -> processMenu());
            });
        }

        if (config().getBoolean("processBody", true)) {
            log.info("processing Sbobet Body...");
            vertx.eventBus().consumer(Address.SYNC_LEAGUE.a, event -> {
                for (LeagueUrl league : gson.fromJson(event.body().toString(), LeagueUrl[].class)) {
                    LeagueUrl cachedLeague = leagueCache.get(league.getUrl());
                    LeagueUrl mergedJson = mergeMenuDayJson(cachedLeague, league);
                    leagueCache.put(league.getUrl(), mergedJson);
                    log.info("merged json with url: " + league.getUrl());
                }
            });

            int bodyInstances = config().getInteger("bodyInstances");
            vertx.deployVerticle("groovy:com.scrapme.core.sbobet.SbobetHtmlBodyVerticle", new DeploymentOptions().setInstances(bodyInstances).setWorker(true), deploymentID -> {
                if (deploymentID.failed()) {
                    throw new RuntimeException("deployment SbobetHtmlBodyVerticle failed", deploymentID.cause());
                }
                vertx.deployVerticle("groovy:com.scrapme.core.sbobet.SbobetParserVerticle", new DeploymentOptions().setInstances(bodyInstances).setWorker(true), deploymentID2 -> {
                    if (deploymentID2.failed()) {
                        throw new RuntimeException("deployment SbobetParserVerticle failed", deploymentID2.cause());
                    }
                    loadMatches(() -> vertx.setPeriodic(config().getInteger("processBodyDelay"), event -> {
                        if (scraperBodyCounter == 0) processBody();
                    }));
                });
            });
        }
    }

    private void loadMatches(CacheLoader loader) {
        mongoService.find("sbobet.url.match", new JsonObject(), event -> {
            if (event.succeeded()) {
                event.result().forEach(json -> matchCache.put(json.getString(KEY), deserializeMatch(json)));
                loader.loaded();
                log.info("Match cache size: " + matchCache.size());
            }
        });
        scraperBodyCounter = 0;
    }

    private Record deserializeMatch(JsonObject json) {
        return gson.fromJson(json.encode(), Record.class);
    }


    private void processMenu() {
        vertx.eventBus().send(Address.SBOBET_MENU.a, null, new DeliveryOptions().setSendTimeout(PROCESS_REPLY_TIMEOUT), this::mergeMenu);
    }

    private void mergeMenu(AsyncResult<Message<Object>> messageAsyncResult) {
        mergeMenu((JsonObject) messageAsyncResult.result().body());
    }

    private void processBody() {

        long now = System.currentTimeMillis();
        if (lastBodyScrapingTime == 0) {
            log.info("start scraping body");
        } else {
            log.info("restart scraping body after:" + (now - lastBodyScrapingTime) + "ms");
        }
        lastBodyScrapingTime = now;
        leagueCache.forEach((key, league) -> {
            if (league.getCount() > 0) {

                JsonObject params = new JsonObject();
                params.put("league", league.getName());
                params.put("sport", league.getCountry().getSport().getName());
                params.put("country", league.getCountry().getName());
                league.getDays().forEach(
                        (date, day) -> {
                            params.put(URL, day.getUrl());
                            scraperBodyCounter++;
                            vertx.eventBus().send(Address.SBONET_SCRAPPER.a, params, new DeliveryOptions().setSendTimeout(PROCESS_REPLY_TIMEOUT), this::scrapperResponse);
                        });
            }
        });

    }

    private void scrapperResponse(AsyncResult<Message<Object>> messageAsyncResult) {
        if (messageAsyncResult.succeeded() && messageAsyncResult.result().body() != null) {
            scraperBodyCounter--;
            vertx.eventBus().send(Address.SBOBET_PARSER.a, messageAsyncResult.result().body(), this::parserResponse);
        } else if (messageAsyncResult.failed()) {
            log.error("scrap body failed", messageAsyncResult.cause());
        }
    }

    public void parserResponse(AsyncResult<Message<Object>> messageAsyncResult) {
        JsonObject msg = (JsonObject) messageAsyncResult.result().body();
        String bodyJson = msg.getString("json", "");
        if (bodyJson.isEmpty()) {
            return;
        }
        Body body = new Gson().fromJson(bodyJson, Body.class);
        for (Market market : body.getMarkets()) {
            if (market.getName().equalsIgnoreCase(oneXtwo)) {
                market.getRecords().forEach(record -> {

                    String key = record.getDate().toString() + ":" + market.getName() + ":" + record.getSideA().getName() + "-" + record.getSideB().getName();
                    matchCache.compute(key, (k, v) -> {
                                String json = null;
                                if (v == null) {
                                    log.info("new match " + key);
                                    json = Transformator.newEventTransform(key, record, market, msg);
                                } else if (v.getDraw() != null && v.getWinA() != null && v.getWinB() != null && !Comparator.oneXTwoEqueal(v, record)) {
                                    log.info("match changed " + key);
                                    json = Transformator.changeEventTransform(key, record, v, market, msg);
                                }
                                if (json != null) {
                                    saveRecord(key, record);
                                    vertx.eventBus().send(Address.CATO_ENDPOINT.a, json);
                                    log.info(json);
                                }
                                return record;
                            }
                    );
                });
                break;

            }
        }
    }

    private void mergeMenu(JsonObject menu) {
        syncLeagueCache(menu.getString("leagues"));
    }

    private void syncLeagueCache(String leagues) {
        vertx.eventBus().publish(Address.SYNC_LEAGUE.a, leagues);
    }

    private void saveRecord(String key, Record record) {
        JsonObject j = new JsonObject();
        j.put("key", key);
        j.put(new Date().getTime() + "", new JsonObject(gson.toJson(record)));
//        mongoService.updateWithOptions("sbobet.url.match", new JsonObject().put("key", key), j, new UpdateOptions().setUpsert(true), event -> {
//            if (event.failed()) {
//                log.error("save match failed", event.cause());
//            }
//        });
        if (record != null && record.getWinA() != null && record.getWinB() != null && record.getDraw() != null)
            mongoService.save("sbobet.url.match", j, event -> {
                if (event.failed()) {
                    log.error("save match failed", event.cause());
                } else {
                    matchCache.put(key, record);
                }
            });
    }

    private LeagueUrl mergeMenuDayJson(LeagueUrl cached, LeagueUrl parsed) {
        if (cached == null) {
            return parsed;
        }
        cached.setCount(parsed.getCount());
        cached.getDays().putAll(parsed.getDays());

        return cached;
    }

}
