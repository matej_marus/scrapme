package com.scrapme.core.sbobet;

import com.google.common.hash.HashFunction;
import com.google.common.hash.Hashing;
import com.scrapme.domain.Market;
import com.scrapme.domain.Record;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;

import java.nio.charset.Charset;

/**
 * Created by matko on 4.1.2015.
 */
public class Transformator {

    private static HashFunction hasher = Hashing.sha256();

    public static String newEventTransform(String key, Record rec, Market market, JsonObject metaData) {
        JsonObject resp = new JsonObject();
        resp.put("id", hasher.newHasher().putString(key, Charset.forName("UTF-8")).hash().asLong());
        resp.put("action", "newEvent");
        resp.put("bookie", "sbobet");
        resp.put("sport", metaData.getString("sport"));
        resp.put("sideA ", rec.getSideA().getName());
        resp.put("sideB", rec.getSideB().getName());
        resp.put("date", rec.getDate().getTime());
        resp.put("league", metaData.getString("league"));

        JsonObject marketJson = new JsonObject();
        marketJson.put("name", market.getName());
        marketJson.put("winA", rec.getWinA());
        marketJson.put("winB", rec.getWinB());
        marketJson.put("draw", rec.getDraw());

        JsonArray markets = new JsonArray();
        markets.add(marketJson);
        resp.put("markets", markets);

        return resp.encode();
    }

    static String changeEventTransform(String key, Record newRec, Record oldRec, Market market, JsonObject metaData) {
        JsonObject resp = new JsonObject();

        resp.put("id", hasher.newHasher().putString(key, Charset.forName("UTF-8")).hash().asLong());
        resp.put("action", "changeEvent");
        resp.put("bookie", "sbobet");
        resp.put("sport", metaData.getString("sport"));
        resp.put("sideA ", newRec.getSideA().getName());
        resp.put("sideB", newRec.getSideB().getName());
        resp.put("date", newRec.getDate().getTime());
        resp.put("league", metaData.getString("league"));

        JsonObject marketJson = new JsonObject();
        marketJson.put("name", market.getName());
        marketJson.put("newWinA", newRec.getWinA());
        marketJson.put("newWinB", newRec.getWinB());
        marketJson.put("newDraw", newRec.getDraw());
        marketJson.put("oldWinA", oldRec.getWinA());
        marketJson.put("oldWinB", oldRec.getWinB());
        marketJson.put("oldDraw", oldRec.getDraw());

        JsonArray markets = new JsonArray();
        markets.add(marketJson);
        resp.put("markets", markets);

        return resp.encode();
    }
}
