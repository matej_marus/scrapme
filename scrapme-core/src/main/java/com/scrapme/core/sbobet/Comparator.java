package com.scrapme.core.sbobet;

import com.scrapme.domain.Record;

import java.util.Objects;

/**
 * Created by matko on 5.12.2014.
 */
public class Comparator {

    public static boolean oneXTwoEqueal(Record cached, Record parsed) {
        return Objects.equals(cached.getWinA(), parsed.getWinA()) && Objects.equals(cached.getWinB(), parsed.getWinB()) && Objects.equals(cached.getDraw(), parsed.getDraw());
    }
}
