package com.scrapme.core;

import io.vertx.core.*;
import io.vertx.core.json.JsonObject;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.impl.LoggerFactory;
import io.vertx.spi.cluster.impl.hazelcast.HazelcastClusterManager;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Iterator;
import java.util.logging.LogManager;
import java.util.stream.Stream;

/**
 * Created by matko on 22.9.2014.
 */
public class Main {

    public static final long DEFAULT_MAX_WORKER_EXECUTE_TIME = 10l * 60 * 1000 * 1000000;
    final static Logger log = LoggerFactory.getLogger(Main.class);
    final static String CONFIG_FILE = "/conf.json";

    public static void main(String[] args) throws URISyntaxException, IOException {
        loadLoggingConfig();
        JsonObject config = new JsonObject(readFromClassPath());
        log.info(config.encodePrettily());
        JsonObject clusterConf = config.getJsonObject("cluster", new JsonObject());
        Vertx.clusteredVertx(new VertxOptions().setClusterManager(new HazelcastClusterManager()).setClusterHost(clusterConf.getString("host", VertxOptions.DEFAULT_CLUSTER_HOST)).setClusterPort(clusterConf.getInteger("port", VertxOptions.DEFAULT_CLUSTER_PORT)).setClustered(true).setMetricsEnabled(true).setHAEnabled(true).setMaxWorkerExecuteTime(DEFAULT_MAX_WORKER_EXECUTE_TIME), new AsyncResultHandler<Vertx>() {
            @Override
            public void handle(AsyncResult<Vertx> event) {
                if (event.succeeded()) {
                    event.result().deployVerticle("java:com.scrapme.core.WebServer", new DeploymentOptions().setConfig(config));
                }
            }
        });

    }

    private static void loadLoggingConfig() throws IOException {
        LogManager.getLogManager().readConfiguration(Main.class.getResourceAsStream("/logging.properties"));
    }

    private static String readFromClassPath() throws URISyntaxException, IOException {
        try {
            readFromPath(Paths.get(CONFIG_FILE));
        } catch (IOException e) {
            ClassLoader ctxClsLoader = Thread.currentThread().getContextClassLoader();
            URL url = null;
            if (ctxClsLoader != null) {
                url = ctxClsLoader.getResource(CONFIG_FILE);
                if (url == null) {
                    url = Main.class.getResource(CONFIG_FILE);
                }
            }
            if (url != null) {
                log.info(url.toString());
                return readFromIS(url.openStream());
            }
        }

        return "{}";
    }

    private static String readFromIS(InputStream is) {
        try (BufferedReader br = new BufferedReader(new InputStreamReader(is))) {
            StringBuilder sb = new StringBuilder();
            String line;
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }
            return sb.toString();
        } catch (IOException e) {
            e.printStackTrace();
            return "{}";
        }
    }

    private static String readFromPath(Path path) throws IOException {
        String conf = "";
        try (Stream<String> lines = Files.lines(path)) {
            Iterator<String> iterator = lines.iterator();
            while (iterator.hasNext()) {
                conf += iterator.next();
            }
        }
        return conf;
    }
}
