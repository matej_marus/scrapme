package com.scrapme.core;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.http.HttpClient;
import io.vertx.core.http.HttpClientOptions;
import io.vertx.core.http.HttpMethod;

/**
 * Created by matko on 16.12.2014.
 */
public class Sender extends AbstractVerticle {


    HttpClient client;

    @Override
    public void start() throws Exception {
        client = vertx.createHttpClient(new HttpClientOptions().setSsl(true).setTrustAll(true).setVerifyHost(false));
        vertx.eventBus().consumer(Address.CATO_ENDPOINT.a, msg -> client.request(HttpMethod.POST, 443, "185.7.63.113", "/scraper/interface.php", handler -> {
            handler.bodyHandler(msg::reply);
        }).putHeader("Content-type", "application/json").end((String) msg.body()));
    }
}
